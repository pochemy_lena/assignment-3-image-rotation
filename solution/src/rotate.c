#include "rotate.h"
#include "image.h"

#include <stdint.h>

static struct image rotate_90(struct image const image){
    struct image new_image = create_image(image.height, image.width);
    for(uint64_t row = 0; row < image.width ; row++){
        for(uint64_t cell = 0; cell < image.height; cell++){
            new_image.data[image.height * row  + cell] = image.data[image.width * cell + (image.width - row - 1)];
        }
    }
    return new_image;
}

static struct image rotate_180(struct image const image){
    struct image new_image = create_image(image.width, image.height);
    for(uint64_t row = 0; row < image.height; row++){
        for(uint64_t cell = 0; cell < image.width; cell++){
            new_image.data[image.width * (image.height - row - 1) + (image.width - cell - 1)] = image.data[image.width * row + cell];
        }
    }
    return new_image;
}

static struct image rotate_270(struct image const image){
    struct image new_image = create_image(image.height, image.width);
    for(uint64_t row = 0; row < image.width ; row++){
        for(uint64_t cell = 0; cell < image.height; cell++){
            new_image.data[image.height * row  + cell] = image.data[image.width * (image.height - cell - 1) + row];
        }
    }
    return new_image;
}

static struct image rotate_0(struct image const image){
    struct image new_image = create_image(image.width, image.height);
    for(uint64_t i = 0; i < image.width * image.height; i++){
        new_image.data[i] = image.data[i];
    }
    return new_image;
}

struct image rotate(struct image image, int const angle){
    int angle_num = ((angle + 360) / 90) % 4;
    switch (angle_num) {
        case 0:
            image = rotate_0(image);
            break;
        case 1:
            image = rotate_90(image);
            break;
        case 2:
            image = rotate_180(image);
            break;
        case 3:
            image = rotate_270(image);
            break;
    }
    return image;
}

