#include "writer.h"
#include "bmp.h"
#include "image.h"

#include <stdio.h>
#include <stdlib.h>


enum write_status to_bmp( FILE* out, struct image* const image){
    uint32_t imageSize = image->height * image->width * sizeof(struct pixel);
    uint64_t fileSize = sizeof(struct bmp_header) + imageSize;
    struct bmp_header new_header = bmp_header_init(fileSize, imageSize, image );

    if(fwrite(&new_header, sizeof(struct bmp_header), 1, out) < 1){
        return WRITE_ERROR;
    }

    uint8_t padding = get_padding(image->width);

    size_t const size_pixel = sizeof(struct pixel);

    uint8_t* pixel = (uint8_t*) malloc(size_pixel);

    if(pixel == NULL) {
        return MEMORY_ERROR;
    }

    uint8_t* padd = malloc(padding);
    if(padd == NULL) {
        free(pixel);
        return MEMORY_ERROR;
    }

    for(uint8_t pad = 0; pad < padding; pad++){
        padd[pad] = 0;
    }

    for(uint32_t row = 0; row < image->height; row++){
        for(uint32_t cell = 0; cell < image->width; cell++){
            struct pixel pix = image->data[image->width * row + cell];
            pixel[0] = pix.b;
            pixel[1] = pix.g;
            pixel[2] = pix.r;
            if(fwrite(pixel, sizeof(struct pixel), 1, out) < 1){
                free(pixel);
                free(padd);
                delete_image(image);
                return WRITE_ERROR;
            }
        }
        if(fwrite(padd, padding, 1, out) < 1){
            free(pixel);
            free(padd);
            delete_image(image);
            return WRITE_ERROR;
        }

    }
    free(pixel);
    free(padd);
    return WRITE_OK;
}
