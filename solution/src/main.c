#include "utils.h"
#include "image.h"
#include "reader.h"
#include "rotate.h"
#include "writer.h"

#include <stdio.h>
#include <stdlib.h>

#define read_error delete_image(&image); \
                close_file(input_file); \
                return 1;


int main( int argc, char** argv ) {

    if(argc != 4){
        perror("Incorrect arguments. Arguments must be: ./image-transformer <source-image> <transformed-image> <angle>");
        return 1;
    }

    int angle = atoi(argv[3]);
    if(!validate_angle(angle)){
        perror("Incorrect angle. Angle must be from list: 0, 90, -90, 180, -180, 270, -270 ");
        return 1;
    }

    struct image image;
    FILE* input_file = open_file(argv[1], "rb");

    switch (from_bmp(input_file, &image)) {
        case READ_INVALID_SIGNATURE:
            printf("File is not bmp file");
            read_error
        case READ_INVALID_HEADER:
            printf("File with incorrect header");
            read_error
        case READ_INVALID_BITS:
            printf("Incorrect bits in file");
            read_error
        case MEMORY_PROBLEM:
            printf("Problem with memory allocation for image");
            read_error
        case READ_OK:
            break;
    }

    close_file(input_file);
    struct image new_image = rotate(image, angle);
    delete_image(&image);

    FILE* output_file = open_file(argv[2], "wb");

    switch (to_bmp(output_file, &new_image)) {
        case WRITE_ERROR:
            printf("Problem with writing to file");
            delete_image(&new_image);
            return 1;
        case MEMORY_ERROR:
            printf("Problem with allocation memory");
        case WRITE_OK:
            delete_image(&new_image);
            break;
    }

    close_file(output_file);
    return 0;
}
