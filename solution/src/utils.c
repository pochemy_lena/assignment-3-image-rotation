#include <stdio.h>

int validate_angle(int const angle){
    int angles[] = {0, 90, -90, 180, -180, 270, -270};
    for (int i = 0; i < sizeof(angles) / sizeof(angles[0]); i++ ){
        if(angle == angles[i]) return 1;
    }
    return 0;
}

FILE* open_file(const char* filename, const char* mode){
    FILE* file = fopen(filename, mode);
    if(!file) {
        perror("Problem with open file");
    }
    return file;
}

void close_file(FILE* file){
    if(file){
        fclose(file);
    }
}



