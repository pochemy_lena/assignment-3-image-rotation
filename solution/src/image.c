#include "image.h"

#include <stdio.h>
#include <stdlib.h>

struct image create_image(uint64_t const width, uint64_t const height){
    struct image new_image = {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
    if(!new_image.data){
        perror("Problem with allocate memory for new image");
        exit(1);
    }
    return new_image;
}

void delete_image(struct image* image){
    if(image->data != NULL){
        free(image -> data);
        image->data = NULL;
    }
}

inline uint8_t get_padding(uint32_t const width){
    return (-1 * width * sizeof(struct pixel)) % 4;
}
