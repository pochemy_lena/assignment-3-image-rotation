#include "bmp.h"


static uint16_t TYPE = 0x4D42;
static uint32_t RESERVED = 0;
static uint16_t BIT_COUNT = 24;
static uint16_t PLANE = 1;
static uint32_t BI_SIZE = 40;
static uint32_t COMPRESSION = 0;


struct bmp_header bmp_header_init(uint64_t const fileSize, uint32_t const imageSize, struct image* const image){
    struct bmp_header new_header = {
            .bfType = TYPE,
            .bfileSize = fileSize,
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = PLANE,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = imageSize,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return new_header;
}

//если что-то не так, вернет 0
int bmp_header_validate(struct bmp_header const *header){
    if(header->bfReserved != RESERVED ||
    header->bOffBits != sizeof(struct bmp_header) ||
    header->biSize != BI_SIZE ||
    header->biWidth < 1 ||
    header->biHeight < 1 ||
    header->biPlanes != PLANE ||
    header->biCompression != COMPRESSION
    ) return 0;

    return 1;
}

int bmp_header_type_validate(struct bmp_header const *header){
    if(header->bfType != TYPE ||
    header->biBitCount != BIT_COUNT ){
        return 0;
    }
    return 1;
}


