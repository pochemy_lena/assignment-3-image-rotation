#include "image.h"
#include "bmp.h"
#include "reader.h"

#include <stdio.h>
#include <stdlib.h>



enum read_status from_bmp( FILE* in, struct image* image){
    struct bmp_header header;

    if(fread(&header, sizeof(struct bmp_header), 1, in) < 1 ||
            !bmp_header_validate(&header) ){
        return READ_INVALID_HEADER;
    }
    if(!bmp_header_type_validate(&header)){
        return READ_INVALID_SIGNATURE;
    }

    *image = create_image(header.biWidth, header.biHeight);
    uint8_t padding = get_padding(header.biWidth);

    size_t const size_pixel = sizeof(struct pixel);

    //uint8_t* pixel = malloc(sizeof(struct pixel));
    uint8_t* pixel = (uint8_t*) malloc(size_pixel);
    if(pixel == NULL) {
//        free(pixel);
        return MEMORY_PROBLEM;
    }

     uint8_t* padd = malloc(padding);
     if(padd == NULL) {
         free(pixel);
         return MEMORY_PROBLEM;
     }

     for(uint32_t row = 0; row < image->height; row++){
         for(uint32_t cell = 0; cell < image->width; cell++){
             if(fread(pixel, sizeof(struct pixel), 1, in) < 1){
                 free(pixel);
                 free(padd);
                 delete_image(image);

                 return READ_INVALID_BITS;
             }
             image->data[image->width * row + cell] = (struct pixel) {
                 pixel[0],
                 pixel[1],
                 pixel[2]
             };
         }
         if(fread(padd, padding, 1, in) < 1){
             free(pixel);
             free(padd);
             delete_image(image);

             return READ_INVALID_BITS;
         }

     }
    free(pixel);
    free(padd);

    return READ_OK;
}
