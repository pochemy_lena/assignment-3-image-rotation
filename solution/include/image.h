#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t const width, uint64_t const height);
void delete_image(struct image* image);
uint8_t get_padding(uint32_t const width);

#endif
