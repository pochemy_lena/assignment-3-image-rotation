#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "reader.h"
#include  <stdint.h>

//
//const uint16_t TYPE = 0x4D42;
//uint32_t RESERVED = 0;
//uint16_t BIT_COUNT = 24;
//uint16_t PLANE = 1;
//uint32_t BI_SIZE = 40;
//uint32_t COMPRESSION = 0;


struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

int bmp_header_validate(struct bmp_header const *header);
int bmp_header_type_validate(struct bmp_header const *header);
struct bmp_header bmp_header_init(uint64_t const fileSize, uint32_t const imageSize, struct image* const image);

#endif
