#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>

int validate_angle(int const angle);

FILE* open_file(const char* filename, const char* mode);

void close_file(FILE* file);

#endif
