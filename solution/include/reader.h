#ifndef READER_H
#define READER_H

#include "image.h"

#include <stdio.h>


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    MEMORY_PROBLEM
};

enum read_status from_bmp( FILE* in, struct image* image);

#endif
