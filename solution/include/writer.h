#ifndef WRITER_H
#define WRITER_H

#include "image.h"

#include <stdio.h>

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    MEMORY_ERROR
};

enum write_status to_bmp( FILE* out, struct image* const image);

#endif
