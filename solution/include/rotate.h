#ifndef ROTATE_H
#define ROTATE_H

#include <stdint.h>

struct image rotate(struct image image, int const angle);

#endif
